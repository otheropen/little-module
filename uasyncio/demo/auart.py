# -*- coding: utf-8 -*-
# @company : Quectel
# @Time    : 2021/9/10 9:43
# @Author  : Jaxsen Xu

import usr.uasyncio as asyncio
from machine import UART

uart = UART(UART.UART2, 115200, 8, 0, 1, 0)


async def sender():
    swriter = asyncio.StreamWriter(uart, {})
    while True:
        swriter.write('hello world\n')
        await swriter.drain()
        await asyncio.sleep(2)


async def receiver():
    sreader = asyncio.StreamReader(uart)
    while True:
        res = await sreader.readline()
        if res:
            print('Recieved', res)


async def main():
    asyncio.create_task(sender())
    asyncio.create_task(receiver())
    while True:
        await asyncio.sleep(1)


def test():
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print('Interrupted')
    finally:
        asyncio.new_event_loop()
        print('as_demos.auart.test() to run again.')


test()
