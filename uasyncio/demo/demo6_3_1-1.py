import usr.uasyncio as asyncio
from machine import UART
uart = UART(UART.UART2, 115200, 8, 0, 1, 0)

async def sender():
    swriter = asyncio.StreamWriter(uart, {})
    while True:
        swriter.write('Hello uart\n')
        await swriter.drain()  # Transmission starts now.
        await asyncio.sleep(2)

async def receiver():
    sreader = asyncio.StreamReader(uart)
    while True:
        res = await sreader.readline()
        print('Received', res)

async def main():
    rx = asyncio.create_task(receiver())
    tx = asyncio.create_task(sender())
    await asyncio.sleep(10)
    print('Quitting')
    tx.cancel()
    rx.cancel()
    await asyncio.sleep(1)
    print('Done')

asyncio.run(main())