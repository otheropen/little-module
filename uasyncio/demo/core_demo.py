# -*- coding: utf-8 -*-
# @company : Quectel
# @Time    : 2021/9/10 15:25
# @Author  : Jaxsen Xu

import uasyncio as asyncio
async def bar(x):
    count = 0
    while True:
        count += 1
        print('Instance: {} count: {}'.format(x, count))
        await asyncio.sleep(2)  # Pause 1s
        print("sleep count instance = {} count = {}".format(x, count))

asyncio.run(bar(1))