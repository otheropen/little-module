# -*- coding: utf-8 -*-
# @company : Quectel
# @Time    : 2021/9/10 9:34
# @Author  : Jaxsen Xu

from usr.uasyncio import Semaphore
import usr.uasyncio as asyncio

async def foo(n, sema):
    print('foo {} waiting for semaphore'.format(n))
    async with sema:
        print('foo {} got semaphore'.format(n))
        await asyncio.sleep_ms(200)

async def main():
    sema = Semaphore()
    for num in range(3):
        asyncio.create_task(foo(num, sema))
    await asyncio.sleep(2)

asyncio.run(main())